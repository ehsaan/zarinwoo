<?php
/**
 * Plugin Name: ZarinWoo
 * Author: Ehsaan
 * Author URI: http://ehsaan.me
 * Description: این افزونه، درگاه پرداخت آنلاین <a href="https://zarinpal.com" target="_blank">زرین‌پال</a> را برای افزونه‌ی <a href="http://woocommerce.com" target="_blank">WooCommerce</a> فعال می‌کند.
 * Version: 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( class_exists( 'WC_Payment_Gateway' ) && ! class_exists( 'WC_ZWoo' ) ):

/**
 * ZarinPal Gateway for WooCommerce
 *
 * @author                      Ehsaan
 * @package                     ZarinWoo
 * @subpackage                  Core
 */
class WC_ZWoo extends WC_Payment_Gateway {
    /**
     * Currencies and their symbols
     * @var                     array
     */
    public $currencies;

    /**
     * Initialize the gateway and hooks.
     * 
     * @return                  void
     */
    public function __construct() {
        $this->currencies = apply_filters( 'zwoo_currencies', [ 'IRR' => 'ریال', 'IRT' => 'تومان', 'IRHR' => 'هزار ریال', 'IRHT' => 'هزار تومان' ] );
        add_filter( 'woocommerce_payment_gateways', array( $this, 'add_gateway' ) );
        add_filter( 'woocommerce_currencies', array( $this, 'add_currencies' ) );
        add_filter( 'woocommerce_currency_symbol', array( $this, 'currency_symbol' ), 10, 2 );

        // Set-up the gateway
        $this->id = 'WC_ZWoo';
        $this->method_title = 'پرداخت امن زرین‌پال';
        $this->method_description = 'درگاه پرداخت امن زرین‌پال برای فروشگاه‌ساز WooCommerce';
        $this->icon = apply_filters( 'zwoo_icon', WP_PLUGIN_URL . '/' . plugin_basename( dirname( __FILE__ ) ) . '/assets/logo.png' );
        $this->has_fields = false;

        $this->init_form_fields();
        $this->init_settings();

        $this->title = $this->settings[ 'title' ];
        $this->description = $this->settings[ 'description' ];
        $this->merchant_code = $this->settings[ 'merchant_code' ];

        if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) )
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        else
            add_action( 'woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options' ) );
        
        add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'process_payment' ) );
        add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'verify' ) );
    }

    /**
     * Make WooCommerce identify this gateway.
     */
    public function add_gateway( $methods ) {
        $methods[] = 'WC_ZWoo';
        return $methods;
    }

    /**
     * Add Iranian currencies to the list
     */
    public function add_currencies( $currencies ) {
        return array_merge( $currencies, $this->currencies );
    }

    /**
     * Return currency symbol per request.
     */
    public function currency_symbol( $symbol, $currency ) {
        $symbol = ( isset( $this->currencies[ $currency ] ) ? $this->currencies[ $currency ] : $symbol );
        return $symbol;
    }

    /**
     * Admin options placeholder
     */
    public function admin_options() {
        parent::admin_options();
    }

    /**
     * Add settings form fields
     */
    public function init_form_fields() {
        $this->form_fields = apply_filters( 'zwoo_settings', array(
            'base_config'               =>  array(
                'title'                 =>  'پیکربندی افزونه',
                'type'                  =>  'title',
                'description'           =>  ''
            ),
            'enabled'                   =>  array(
                'title'                 =>  'فعال‌بودن افزونه؟',
                'type'                  =>  'checkbox',
                'label'                 =>  'برای فعال‌کردن درگاه زرین‌پال برای WooCommerce این گزینه را فعال کنید.',
                'default'               =>  'yes',
                'desc_tip'              =>  true
            ),
            'title'                     =>  array(
                'title'                 =>  'نام درگاه',
                'type'                  =>  'text',
                'description'           =>  'نام درگاه را مشتریان در مرحله‌ی پرداخت مشاهده می‌کنند.',
                'default'               =>  'پرداخت امن زرین‌پال (با کلیه‌ی کارت‌های شتاب)',
                'desc_tip'              =>  true
            ),
            'description'               =>  array(
                'title'                 =>  'توضیحات درگاه',
                'type'                  =>  'text',
                'description'           =>  'توضیحات درگاه را مشتریان در مرحله‌ی پرداخت مشاهده می‌کنند.',
                'default'               =>  'پرداخت امن به وسیله‌ی کارت‌های عضو شبکه‌ی شتاب از طریق درگاه زرین‌پال',
                'desc_tip'              =>  true
            ),
            'gateway_config'            =>  array(
                'title'                 =>  'پیکربندی درگاه',
                'type'                  =>  'title',
                'description'           =>  ''
            ),
            'merchant_code'             =>  array(
                'title'                 =>  'مرچنت‌کد درگاه',
                'type'                  =>  'text',
                'description'           =>  'مرچنت‌کد خود را پس از عضویت نقره‌ای و ثبت‌کردن فروشگاه‌تان دریافت می‌کنید.',
                'default'               =>  '',
                'desc_tip'              =>  true
            )
        ) );
    }

    /**
     * Process the payment.
     */
    public function process_payment( $order_id ) {
        global $woocommerce;

        $woocommerce->session->zwoo_order = $order_id;
        $order = new WC_Order( $order_id );
        $currency = $order->get_order_currency();

        $amount = ( int ) $order->order_total;
        switch( strtolower( $currency ) ) {
            case 'irht':
                $amount *= 1000;
                break;
            case 'irhr':
                $amount *= 100;
                break;
            case 'irr':
                $amount /= 10;
                break;
        }

        $merchant = $this->merchant_code;
        $callback = add_query_arg( 'wc_order', $order_id, WC()->api_request_url( 'WC_ZWoo' ) );

        $desc = 'خرید به شماره‌ی سفارش #' . $order->get_order_number();
        $mobile = ( $mobile = get_post_meta( $order_id, '_billing_phone', true ) ) ? $mobile : '';
        $email = $order->billing_email;
        
        $data = json_encode( array(
            'MerchantID' 			=>	$merchant,
            'Amount' 				=>	$amount,
            'Description' 			=>	$desc,
            'CallbackURL' 			=>	$callback,
            'Mobile'                =>  $mobile,
            'Email'                 =>  $email
        ) );

        $ch = curl_init( 'https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json' );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1' );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json', 'Content-Length: ' . strlen( $data ) ] );
            
        $result = curl_exec( $ch );
        $err = curl_error( $ch );
        curl_close( $ch );

        if ( $err ) {
            $note = 'خطا هنگام ارسال به درگاه. کد خطا: CURL#' . $err;
            $order->add_order_note( $note );
            return [ 'result' => 'failure', 'message' => 'CURL#' . $err ];
        }

        $result = json_decode( $result, true );
        if ( $result['Status'] == 100 ) {
            $order->add_order_note( 'کد تراکنش زرین‌پال: ' . $result['Authority'] );
            $woocommerce->session->zwoo_authority = $result['Authority'];
            // TODO: add an option for switching between WebGate and ZarinGate
            if ( ! headers_sent() ) {
                header( 'Location: ' . sprintf( 'https://www.zarinpal.com/pg/StartPay/%s/ZarinGate', $result['Authority'] ) );
                exit;
            } else {
                echo 'در حال انتقال به درگاه بانکی...';
                echo '<script type="text/javascript">window.onload = function(){top.location.href = "' . sprintf( 'https://www.zarinpal.com/pg/StartPay/%s/ZarinGate', $result['Authority'] ) . '";};</script>';
                exit;
            }
        } else {
            $note = 'خطا هنگام ارسال به درگاه. کد خطا: ZP#' . $result['Status'];
            $order->add_order_note( $note );
            return [ 'result' => 'failure', 'message' => 'ZP#' . $result['Status'] ];
        }
    }

    /**
     * Verify transaction afterwards.
     */
    public function verify() {
        global $woocommerce;
        $order_id = ( $order_id = $woocommerce->session->zwoo_order ) ? $order_id : false;
        if ( ! $order_id ) wp_die( 'No order_id in session. Probably a dead session. Try again.' );
        $woocommerce->session->zwoo_order = false;

        $order = new WC_Order( $order_id );
        $currency = $order->get_order_currency();

        if ( $order->status == 'completed' ) wp_die( 'Transaction is already completed.' );

        $merchant = $this->merchant_code;
        if ( $_GET['Status'] != 'OK' ) wp_die( 'Received failure from ZarinPal.' );

        $amount = ( int ) $order->order_total();
        switch( strtolower( $currency ) ) {
            case 'irht':
                $amount *= 1000;
                break;
            case 'irhr':
                $amount *= 100;
                break;
            case 'irr':
                $amount /= 10;
                break;
        }

        $authority = $_GET['Authority'];

        $data = json_encode( array(
            'MerchantID' 			=>	$merchant,
            'Amount' 				=>	$amount,
            'Authority'				=>	$authority
        ) );

        $ch = curl_init( 'https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json' );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1' );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json', 'Content-Length: ' . strlen( $data ) ] );
        
        $result = curl_exec( $ch );
        curl_close( $ch );
        $result = json_decode( $result, true );
        
        if ( $result['Status'] == 100 ) {
            $order->payment_complete( $result['RefID'] );
            $woocommerce->cart->empty_cart();
            $order->add_order_note( 'Ref ID: ' . $result['RefID'] );
            wp_redirect( add_query_arg( 'wc_status', 'success', $this->get_return_url( $order ) ) );
            exit;
        } else {
            $order->add_order_note( 'Error: ' . $result['Status'] );
            wp_redirect( $woocommerce->cart->get_checkout_url() );
            exit;
        }
    }
}

endif;

add_action( 'plugins_loaded', function() {
    new WC_ZWoo();
}, 0 );
